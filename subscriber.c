#include <stdio.h>
#include <nats.h>
#include <getopt.h>
#include <sys/stat.h>
#include "data.h"
#include <pthread.h>


char nats_url[100] = "nats://localhost:4222";
char subject[100] = "foo";
char filename[100] = "";
int verbose_flag = 0;
int verbose_id = -1;
int writing_flag = 0;
int message_count = 0;
pthread_t thread_id;

FILE *fp;
char path[100];

struct MessageData *allMessages; //TODO: remove


/**
 * prints help
 */

void printHelp() {
    printf("--verbose id\tprints messages to console.\n");
    printf("--subject m:\tdefines a subject. (default foo)\n");
    printf("--output m\tfilename of the output.(stored in csv)\n");
    printf("--url m:\tdefines a url to a specific nats server. (default nats://localhost:4222)\n");
}


void writeToFile(int from, int to) {

    writing_flag = 1;
    if (verbose_flag) {
        printf("id %d; from %d to %d\n", verbose_id, from, to);
    }
    if (strlen(filename) == 0)
        return;

    FILE *fp;
    mkdir("../results", 0777);
    char path[100];
    sprintf(&path, "../results/%s.csv", filename);

    if (from == 0) {
        fp = fopen(path, "w");
    } else {
        fp = fopen(path, "a");
    }

    if (fp == NULL) {
        printf("Failed to open file");
        exit(1);
    }

    if (from == 0) {
        fprintf(fp, "Number,total,start,end\n");
    }

    for (int i = from; i < to; i++) {
        struct MessageData data = allMessages[i];
      //  fprintf(fp, "%ld,%d,{ \"sec\": %ld; \"usec\":%d },{ \"sec\": %ld; \"usec\":%d }\n", data.number, (int) data.total, data.start.tv_sec, data.start.tv_usec, data.start.tv_sec, data.start.tv_usec);
        if (verbose_flag) {
            // printf("id %d; write %ld,%d idx:%d to: %d\n", verbose_id, data.number, (int) data.total, i, to);
        }
    }

    fflush(fp);
    fclose(fp);
    writing_flag = 0;
}

/**
 * onMessage handler
 * @param nc
 * @param sub
 * @param msg
 * @param closure
 */
static void onMsg(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure) {

    static int init = 0;
    static int cnt = 0;
    static int refRate;
    static int _refID[1024];
    static struct timeval tv0, tv1;
    static long _dmin = 1000 * 1000 * 1000;
    static long _dmax = 0;
    static long long _dsum = 0;


    struct MessageData data;
    memcpy(&data, natsMsg_GetData(msg), natsMsg_GetDataLength(msg));
    //calculateSendTime(&data);


    if (verbose_flag) {
        printf("id %d; Received msg: %ld %d\n", verbose_id, data.number, message_count);
    }

    if (!init) {
        init = 1;
        refRate = data.refRate;
        gettimeofday(&tv0, NULL);
        printf("Target Rate %d.\n", refRate);
        fflush(stdout);
    }

    int seconds = 1;
    message_count++;
    
    if (++cnt >= seconds * refRate) {
        gettimeofday(&tv1, 0);
        int delay = (tv1.tv_sec - tv0.tv_sec) * 1000 +
                    (tv1.tv_usec - tv0.tv_usec) / 1000;

        if (!delay) delay = 1;

        long davg = _dsum / cnt;

        printf("%u;\tDelay: %5d ms/%d Msg; Rate %2f Msg/s; Latency: min %5ld us, max %ld us, avg %5ld us; count %d cnt %d expected\n",
               (unsigned) time(NULL), delay, seconds * refRate, (double) (seconds * refRate * 1000) / delay, _dmin,
               _dmax, davg,
               message_count, cnt);
        if (fp != NULL) {
            fprintf(fp, "%d,Delay %5d ms/%d Msg,%2f,%5ld,%ld,%5ld\n",
                    refRate, delay, seconds * refRate, (double) (seconds * refRate * 1000) / delay, _dmin, _dmax, davg);
            fflush(fp);
        }
        fflush(stdout);

        tv0 = tv1;
        cnt = 0;
        _dmin = 1000 * 1000 * 1000;
        _dmax = 0;
        _dsum = 0;
    }

    int trate = data.refRate;                   // temporary rate

    if (trate != refRate) {
        printf("    erhalten: %d, erwartet: %d", cnt, 30 * refRate);
        printf("    SEND RATE CHANGED TO %d Msg/s.\n", trate);
        fflush(stdout);
        refRate = trate;
    }

    struct timeval tv;
    gettimeofday(&tv, 0);

    long delay = (tv.tv_sec - data.start.tv_sec) * 1000 * 1000 + tv.tv_usec - data.start.tv_usec;

    if (delay < _dmin) _dmin = delay;
    if (delay > _dmax) _dmax = delay;

    _dsum += delay;


    // Need to destroy the message!
    natsMsg_Destroy(msg);
}


void startSubscribe(char *subject) {
    natsConnection *conn = NULL;
    natsSubscription *sub = NULL;
    natsStatus s;
    volatile bool done = false;

    printf("Listening on subject '%s'\n", subject);

    // Creates a connection to the default NATS URL
    s = natsConnection_ConnectTo(&conn, nats_url);

    if (s == NATS_OK) {
        s = natsConnection_Subscribe(&sub, conn, subject, onMsg, (void *) &done);
    }
    if (s == NATS_OK) {
        for (; !done;) {
            //nats_Sleep(0);
        }
        natsSubscription_Unsubscribe(sub);
        // Anything that is created need to be destroyed
        natsSubscription_Destroy(sub);
        natsConnection_Destroy(conn);
    }


    printf("id %d; received %d messages\n", verbose_id, message_count);
    // If there was an error, print a stack trace and exit
    if (s != NATS_OK) {
        nats_PrintLastErrorStack(stderr);
        exit(2);
    }
}

/**
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
    int c;

    while (1) {
        static struct option long_options[] = {
                {"subject", required_argument, 0, 's'},
                {"help",    required_argument, 0, 'h'},
                {"count",   required_argument, 0, 'c'},
                {"output",  required_argument, 0, 'o'},
                {"verbose", required_argument, 0, 'v'},
                {"url",     required_argument, 0, 'u'}
        };

        int option_index = 0;

        c = getopt_long(argc, argv, "s:h:c:o:v:u", long_options, &option_index);

        if (c == -1) {
            break;
        }

        switch (c) {
            case 0:
                if (long_options[option_index].flag != 0)
                    break;
                printf("option %s", long_options[option_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
                break;
            case 's':
                sprintf(subject, "%s", optarg);
                break;
            case 'o':
                sprintf(filename, "%s", optarg);
                break;
            case 'h':
                printHelp();
                break;
            case 'v':
                printf("verbose\n");
                verbose_id = atoi(optarg);
                verbose_flag = 1;
                break;
            case 'u':
                sprintf(nats_url, "%s", optarg);
                break;
            case '?':
            default:
                printf("Unknown option `-%c'.\n", optopt);
                printHelp();
                break;

        }
    }

    printf("Trying to connect to: %s\n", nats_url);

    sprintf(path, "../%u-subscriber-nats.csv", (unsigned) time(NULL));
    fp = fopen(path, "w");
    //Delay ms/int msgs
    //rate msg/s
    //latency min - us
    //latency max
    //latency avg
    if (fp == NULL) {
        printf("cannot open file\n");
    } else {
        fprintf(fp, "TargetRate,Delay,rate msg/s,lateny min us,latency max us, latency avg us\n"); //TODO:
        fflush(fp);
    }

    startSubscribe(subject);
    fclose(fp);
    return 0;
}