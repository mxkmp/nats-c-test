//
// Created by Maximilian Kamp on 20.03.20.
//

#ifndef NATS_TEST2_DATA_H
#define NATS_TEST2_DATA_H

#include <sys/time.h>
#include <string.h>

//614 size of
struct MessageData {
    char message[564];
    char appName[16];

    long number;
    int refRate;
    struct timeval start;
};

uint64_t elapsedTime(struct timeval st, struct timeval et) {
   return ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec);
}

struct MessageData createMessage(int rate, long number) {
    struct MessageData data;
    struct timeval time;

    data.number = number;
    data.refRate = rate;
    gettimeofday(&time, 0);
    data.start = time;
    return data;
}

void calculateSendTime(struct MessageData *data) {
    struct timeval time;
    gettimeofday(&time, (void*)NULL);
}


#endif //NATS_TEST2_DATA_H
