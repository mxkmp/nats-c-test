#Tests
Versuchsaufbau:
Senden von N Nachrichten gestaffelt in 5 Schritte:

* 100
* 1000 
* 10000
* 100000
* 1000000

Hier berechnen wie viele Nachrichten es die Sekunde sind. Nachrichten Größe sehr wichtig.

https://dzone.com/articles/benchmarking-message-queue-latency

Gestaffelt in Nachrichtengröße
* 256 Byte 3,000 requests/sec (768 KB/s)
* 1 KB 3,000 requests/sec (3 MB/s)
* 5 KB 2,000 requests/sec (10 MB/s)
* 1 KB 20,000 requests/sec (20.48 MB/s)
* 1 MB 100 requests/sec (100 MB/s)

Format der Nachricht: 

{
    number: int,
    message: byte[],
    send: time, 
    received: time
}

Nachrichten werden gesammelt und anschließend in einer CSV Datei persistiert. 

Auswertung erfolgt durch ein zusätzliches Skript. 
Dieses gibt an die durchschnittliche Übertragung sowie die gesamte Übertragungsdauer an.


Anschließend Vergleich vs Cluster -> eher nicht


N Subscriber / Publisher in einem Diagramm vergleichen

TODO:

N - 1 -> Problem ist, dass die Nachrichten nicht eindeutig zugeordnet werden können, da die Zahl die Nummer n der  Nachrichten voneinander abweicht -> ID des Publsihers hinzufügen

# 1 - 1
Eine Topic ein Subscriber

# 1 - n

Eine Topic n Subscriber

# n - 1

Eine Topic n Publisher und 1 Subscriber

# n - n

N Topics n Publisher N Subscriber



# TODOS: 

Testen
Auswertung der Daten:

Wie viele Nachrichten schafft der Subscriber wirklich die Sekunde?

Jeder Subscriber bekommt alle Daten.
Von allen Subscribern wird alles zusammen gefasst und der Durchschnitt erfasst.

Start und ende geben an wie lange der gesamte Test war. Ausprobieren.

In N Teile unterteilen. 

Negative Zahlen Warum?


Durchschnitt aller Subscriber des Tests erfassen. 
Eventuell auch die Streuung ermitteln.


## Was brauche ich für Daten aus den Daten: 
X Achse Zeit

Y Achse Latency

Durchschnittliche Zeit

Streuung 

Angaben der Zeit in Sekunden ab t 0



Verbesserung: 

Subscriber leert seinen Speicher erst wenn er keine Nachrichten mehr bekommt.

