#include <stdio.h>
#include <nats.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include "data.h"

static int verbose_flag;
int verbose_id = -1;
char message[100] = "tesMesstage";
char subject[100] = "foo";
char nats_url[100] = "nats://localhost:4222";
int duration = 5;
int minStep = 10000;
int maxStep = 20000;
int steps = 10000;


natsConnection *conn = NULL;
natsStatus s;


/**
 * prints help
 */
void printHelp() {
    printf("--verbose id\tprints messages to console.\n");
    printf("--subject m:\tdefines a subject. (default foo)\n");
    printf("--duration s:\tdefines the length of the benchmark test. (default 5)\n");
    printf("--url m:\tdefines a url to a specific nats server. (default nats://localhost:4222)\n");
    printf("--min n:\t defines minimum messages per second\n");
    printf("--max n:\t defines maximum messages per second\n");
    printf("--step n:\t defines the step interval\n");
}


natsStatus connectToNATS() {
    return natsConnection_ConnectTo(&conn, nats_url); //TODO: durchgehende verbindung möglich?
}

void disconnect() {
    // Anything that is created need to be destroyed
    natsConnection_Destroy(conn);
}

void sendMessageData(char *subject, struct MessageData *data) {
    natsMsg *msg = NULL;

    if(verbose_flag) {
        printf("id: %d; Publishes message: %d on subject '%s'\n", verbose_id, data->number, subject);
    }

    // Creates a connection to the default NATS URL
    if (s == NATS_OK) {
        s = natsMsg_Create(&msg, subject, NULL, (void*)data, sizeof(*data));
    }
    if (s == NATS_OK) {
        // Publishes the message on subject "foo".
        s = natsConnection_PublishMsg(conn, msg);
    }
    natsMsg_Destroy(msg);

    // If there was an error, print a stack trace and exit
    if (s != NATS_OK) {
        nats_PrintLastErrorStack(stderr);
        exit(s);
    }
}

void printTime() {
    char buffer[26];
    int millisec;
    struct tm *tm_info;
    struct timeval tv;

    gettimeofday(&tv, NULL);

    millisec = lrint(tv.tv_usec / 1000.0); // Round to nearest millisec
    if (millisec >= 1000) { // Allow for rounding up to nearest second
        millisec -= 1000;
        tv.tv_sec++;
    }

    tm_info = localtime(&tv.tv_sec);

    strftime(buffer, 26, "%Y:%m:%d %H:%M:%S", tm_info);
    printf("%s.%03d\n", buffer, millisec);
    fflush(stdout);
}

void sleepTill(long long till) {
    struct timeval now;
    gettimeofday(&now, NULL);

    long long tnow = 1000LL * (1000LL * 1000 * now.tv_sec + now.tv_usec);
    long long twait = till - tnow;

    if (twait <= 0)
        return;

    // on long delays: do actually sleep some time
    if (twait > 1001LL * 1000 * 1000)
        usleep(twait / 1000 - 1000);

    // finally perform a busy loop
    for (;;) {
        gettimeofday(&now, NULL);

        long long tnow = 1000LL * (1000LL * 1000 * now.tv_sec + now.tv_usec);
        long long twait = till - tnow;

        if (twait < 1000)
            break;
    }
}

/**
 * @param seconds
 * @param messages
 * @param message_length
 */
void startTimer(int seconds, int messages_per_second) {
    struct timeval start, next, stop, reference;
    gettimeofday(&start, NULL);

    stop.tv_sec = start.tv_sec + (seconds * ((maxStep - minStep) / messages_per_second));
    stop.tv_usec = start.tv_usec + 200000; // end time now + 200ms

    gettimeofday(&reference, NULL);

    int maxBlocks = messages_per_second / 5 * 1;
    if (!maxBlocks) maxBlocks = 1;

    int maxMessages = seconds * messages_per_second;
    //Intervalle, die wir definieren abhängig von der Zeit
    long long tsend = 1000LL * (1000LL * 1000 * reference.tv_sec + reference.tv_usec);
    long long interval = 1000LL * 1000 * 1000 * 1 / messages_per_second;

    int cnt = 0;
    for (;;) {
        if (reference.tv_sec >= stop.tv_sec && reference.tv_usec > stop.tv_usec || cnt == maxMessages) {
            break;
        }
        //send data
        struct MessageData data = createMessage(messages_per_second, ++cnt);
        sendMessageData(subject, &data);

        //send message
        tsend += interval;
        sleepTill(tsend); //sleep till next message;

        // recalculate block delay and medium delay
        struct timeval tp;
        gettimeofday(&tp, NULL);
        reference = tp;
    }

    //nachrichten die sekunde + länge
//    printf("msg/s %d, duration: %d, message length %lu bytes \n", overall_count / seconds, seconds,sizeof(struct MessageData));
    fflush(stdout);
}


int main(int argc, char **argv) {
    int c;

    while (1) {
        static struct option long_options[] = {
                {"min",      required_argument, 0, 'm'},
                {"subject",  required_argument, 0, 's'},
                {"help",     required_argument, 0, 'h'},
                {"url",      required_argument, 0, 'u'},
                {"verbose",  required_argument, 0, 'v'},
                {"duration", required_argument, 0, 'd'},
                {"max",      required_argument, 0, 'n'},
                {"steps",    required_argument, 0, 't'},
        };

        int option_index = 0;


        c = getopt_long(argc, argv, "m:s:h:u:v:d:n:t", long_options, &option_index);

        if (c == -1) {
            break;
        }

        switch (c) {
            case 0:
                if (long_options[option_index].flag != 0)
                    break;
                printf("option %s", long_options[option_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
                break;
            case 's':
                sprintf(subject, "%s", optarg);
                break;
            case 'u':
                sprintf(nats_url, "%s", optarg);
                break;
            case 'v':
                verbose_flag = 1;
                verbose_id = atoi(optarg);
                break;
            case 'd':
                duration = atoi(optarg);
                break;
            case 'm':
                minStep = atoi(optarg);
                break;
            case 'n':
                maxStep = atoi(optarg);
                break;
            case 't':
                steps = atoi(optarg);
                break;
            case 'h':
                printHelp();
                break;
            case '?':
            default:
                printf("Unknown option `-%c'.\n", optopt);
                printHelp();
                break;

        }
    }

    if ((s = connectToNATS()) != NATS_OK) {
        nats_PrintLastErrorStack(stderr);
        return s;
    }
    printf("Publisher started with id: %d. writing on subject %s\nTrying to connect to url: %s\n",
           verbose_id, subject, nats_url, duration);

    for (int i = minStep; i <= maxStep; i += steps) {
        printf("Sending %d m/s for %d seconds\n", i, duration);
        startTimer(duration, i);
        sleep(1);
    }
    disconnect();

    printf("Publisher finished.\n");

    return 0;
}


