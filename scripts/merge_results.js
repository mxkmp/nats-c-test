#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

const resultPath = '../results'
const verbose = false;

function parseMatchedInt(str) {
    if(!str) return str;
    str = str.replace('_', '');
    return parseInt(str);
}
class CSVRootObject {
    children = [];

    constructor(type, count, n, m, isCluster, number) {
        this.type = type;
        this.count = count;
        this.n = n;
        this.m = m;
        this.isCluster = isCluster;
    }

    generateGeneralName() {
        return `${this.type}_${this.count}${this.n ? '_' + this.n : ''}${this.m ? '_' + this.m : ''}${this.isCluster ? '_cluster' : ''}`
    }

    reformatTimeToMS(timestruct){
        const {sec, usec} = JSON.parse(timestruct.replace(";",","))
        return sec * 1000 + usec / 1000;
    }

    collectAllInformationsFromCSV() {
        let allLines = []
        let header;


        //TODO: alle children lesen und zusammenfassen
        const child = this.children[0];

        const lines = child.readFile();
        const lines2 = []
        const sumFaktor = lines.length / 1000

        //aggrate data for one children
        let aggLines = []
        let cnt = 0;
        let cnt2 = 0;

        let t0 = this.reformatTimeToMS(lines[0].start);
        let t1;

        do {
            //TODO: entfernen
            cnt2++;
            console.log('cnt', cnt2);
            let sliceLength;
            if(cnt >= lines.length) {
                break;
            }
            if(cnt + sumFaktor > lines.length) {
                sliceLength = lines.length;
            } else {
                sliceLength = cnt + sumFaktor
            }

            const slicedLines = lines.slice(cnt, sliceLength);

            const aggregated = slicedLines.reduce((acc, cur, idx) => {
                if(!acc.hasOwnProperty("max")) {
                    acc.max = 0;
                }
                if(!acc.hasOwnProperty("min")) {
                    acc.min = Number.MAX_SAFE_INTEGER;
                }

                if(idx === 1) {
                    /*const {sec, usec} = JSON.parse(acc.start.replace(";",","))
                    //TODO: Fehler es fehlt wirkliches t0
                    acc.start = sec * 1000 + usec / 1000;
                    acc.start = acc.start*/
                } else if(idx === slicedLines.length -1) {
                    acc.end = this.reformatTimeToMS(cur.end)
                    acc.number = parseInt(acc.end - t0)
                }

                if(acc.max < cur.total) {
                    acc.max = parseInt(cur.total);
                } else if (acc.min > cur.total) {
                    acc.min = parseInt(cur.total);
                }

                acc.total = parseInt(acc.total) + parseInt(cur.total)
                return acc;
            })
            aggregated.total = aggregated.total / 1000;
            aggLines.push(aggregated);

            cnt+= sumFaktor
        } while(cnt < lines.length + sumFaktor)


        allLines = aggLines;

        const resultDir = resultPath + '/merged/';
        if(!fs.existsSync(resultDir)) {
            fs.mkdirSync(resultDir);
        }
        console.log( {allLines: allLines.length, name: this.generateGeneralName(), children: this.children.length});

        allLines = allLines.map ( e => e.number + ',' + e.total + ',' + e.min + ',' + e.max);
        allLines.unshift('number,total,min,max');

        fs.writeFileSync(
            resultDir + this.generateGeneralName() + '.csv',
            allLines.join('\n'),
            {encoding: 'utf-8', flag: 'w'});
    }
}

/**
 *
 */
class CSVObject {
    lines = [];
    header;

    constructor(path, name,  number) {
        this.path = path;
        this.name = name;
        this.number = number;
    }

    readFile() {
        const lines = fs.readFileSync(this.path + '/' + this.name, 'utf-8').split("\n")
        this.header = lines.shift();
        for(const line of lines) {
            const [number, total, start, end] = line.split(',');
            if(!number || number.length == 0) continue;
            this.lines.push({number, total, start, end});
        }
        return this.lines;
    }
}


let allTypes;

/**
 *
 * @param files
 * @returns {Map<String, CSVRootObject>}
 */
function collectSimiliarFiles(files) {
    //const regex = new RegExp(/([\w|\d]_[\w|\d])_(\d+)_(\d+)(_cluster)?_(\d+).csv/);
   // const regex = new RegExp(/([\w|\d]-[\w|\d])(_\d+)([_\d]{1,3}([_\d]){1,3}?)?(_cluster)?(_\d{1,3})(\.csv)/);
    const regex = new RegExp(/([\w|\d]-[\w|\d])(_\d+)(_[\d]{1,3})?(_[\d]{1,3})?(_[\d]{1,3})(_cluster)?(_\d{1,3})(\.csv)/);
    const allTypes = new Map();

    //collect data
    for(const file of files) {
        const exec = regex.exec(file);
        console.log(file);
        if(exec !== null) {

            //TODO: hier bekomme ich alle children
            //TODO: auslesen und durchschnitt bilden + Sekunden zählen.
            //types: 1_1, 1_N, N_1, N_N
            let [name, type, count, n, m, isCluster, number] = exec;
            count = parseMatchedInt(count);
            n = parseMatchedInt(n);
            m = parseMatchedInt(m);
            number = parseMatchedInt(number);
            const root = new CSVRootObject(type, count, n, m, isCluster, number);
            const obj = new CSVObject(resultPath, name, number);
            if(verbose) {
                console.log(root.generateGeneralName(), file, exec);
            }

            let rootObject = allTypes.get(root.generateGeneralName());

            if(rootObject) {
                rootObject.children.push(obj)
                allTypes.set(root.generateGeneralName(), rootObject);
            } else {
                root.children.push(obj);
                allTypes.set(root.generateGeneralName(), root);
            }
        }
    }

    return allTypes;
}

function getAllResultFiles() {
    const directoryPath = path.join(__dirname, resultPath);

    fs.readdir(directoryPath,  (err, files) => {
        //console.log('files', files);
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }
        allTypes = collectSimiliarFiles(files);
        for(const value of allTypes.values()) {
            value.collectAllInformationsFromCSV();
        }
    });

}

getAllResultFiles();