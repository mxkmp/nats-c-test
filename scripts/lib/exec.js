
/**
 * Executes shell command as it would happen in BASH script
 * @param {string} command
 * @param {Object} [options] Object with options. Set `capture` to TRUE, to capture and return stdout.
 *                           Set `echo` to TRUE, to echo command passed.
 * @returns {Promise<{code: number, data: string | undefined, error: Object}>}
 */
const exec = function (command, {capture = false, echo = false} = {}) {
  if (echo) {
    console.log(command);
  }

  const spawn = require('child_process').spawn;
  const childProcess = spawn('bash', ['-c', command], {stdio: capture ? 'pipe' : 'inherit'});

  return new Promise((resolve, reject) => {
    let stdout = '';

    if (capture) {
      childProcess.stdout.on('data', (data) => {
        stdout += data;
      });
    }

    childProcess.on('error', function (error) {
      reject({code: 1, error: error});
    });

    childProcess.on('close', function (code) {
      if (code > 0) {
        reject({code: code, error: 'Command failed with code ' + code});
      } else {
        resolve({code: code, data: stdout, pid: childProcess.pid, childProcess: childProcess});
      }
    });
  });
};

/**
 * sleeps time seconds
 * @param time in seconds
 * @returns {Promise<unknown>}
 */
const sleep = function (time) {
  return new Promise((resolve) => setTimeout(resolve, time * 1000));
}

const startNatsSingle = async function () {
  return await exec('docker run -d --name nats_server -p 4222:4222 -p 6222:6222 -p 8222:8222 nats:alpine3.10').catch(err => console.error('startNats', err));
}

const startNatsCluster = async function (shell) {
  const processes = []
  processes.push(shell.exec("xterm -e nats-server -p 4222  -cluster nats://localhost:4248 -D", {async: true}))
  processes.push(shell.exec("xterm -e nats-server -p 5222 -cluster nats://localhost:5248 -routes nats://localhost:4248 -D", {async: true}))
  processes.push(shell.exec("xterm -e nats-server -p 6222 -cluster nats://localhost:6242 -routes nats://localhost:4248 -D", {async: true}));
  return processes;
}

const killAll = async function() {
  await exec("pkill -9 nats_subscriber").catch(err => console.log('kill sub', err));
  await exec("pkill -9 nats_publisher").catch(err => console.log('kill pub', err));
  await exec("docker kill nats_server").catch(err => console.log('kill nats_server docker', err));
  await exec("pkill -9 nats-server").catch(err => console.log('kill nats_server', err));
  await exec("pkill -9 xterm").catch(err => console.log('kill xterm', err));
}

module.exports = {
  exec, killAll, sleep, startNatsSingle, startNatsCluster
}