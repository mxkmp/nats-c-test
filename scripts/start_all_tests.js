#!/usr/bin/env node


const {exec, sleep, killAll, startNatsSingle, startNatsCluster} = require('./lib/exec')
const shell = require('shelljs');
let verbose = false;

let natsUrl = "nats://cefdyin1:360222"

const allNumbers = [/*100, 1000, 10000, */100000, 1000000];
const allNs = [/*2, 4, 8, 16, 32, 64, */128, 256]; //TODO: eventuell noch mehr

let isCluster = true;

const duration = 30;
let min = 10000;
let max = 50000;
let steps = 10000;
let argSwitch = null;


/**
 * stops if closure returns true
 * @param closure
 * @param delay
 */
function timer(closure, delay) {
    setTimeout(() => {
        const ret = closure();
        if (ret) return;
        timer(closure, delay);
    }, delay)
}

const delay = ms => new Promise(res => setTimeout(res, ms));

/*
only works for subscriber
 */
async function waitForProcessesToBeLoaded(allProcesses) {
    return new Promise(resolve => {
        require('events').EventEmitter.defaultMaxListeners = Infinity;
        let length = allProcesses.length;
        for (let i = 0; i < allProcesses.length; i++) {
            console.log('process', i);
            allProcesses[i].stdout.on('data', (data) => { //first message -> process is loaded
                console.log('data', data);
                length--;
            })
            allProcesses[i].stderr.on('data', (err) => {
                console.log('err', err);
                length--;
            })
            allProcesses[i].on('close', (err) => {
                console.log('close', err);
                length--;
            })
        }

        timer(() => {
            if (length === 0) {
                resolve(true);
            }
        }, 1000)
    })
}

/*

 */
async function waitForProcessesToFinish(allProcesses) {
    return new Promise((resolve, reject) => {
        require('events').EventEmitter.defaultMaxListeners = Infinity;

        let length = allProcesses.length;
        for (let i = 0; i < allProcesses.length; i++) {
            allProcesses[i].setMaxListeners(0)
            allProcesses[i].on('close', (code) => {
                if (code > 0) {
                    console.error('error closing', code, allProcesses);
                    reject(code);
                }
                length--;
            })

            timer(() => {
                if (length === 0) {
                    resolve(true);
                }
            }, 1000)
        }
    });
}

function genOutputFolder(name, count, duration, n, m) {
    name = name + '_' + count + '_' + duration;
    if (n) {
        name += '_' + n;
    }
    if (m) {
        name += '_' + m;
    }
    return isCluster ? name + '_cluster' : name;
}

function genSubject(name, count, n, m) {
    let subject = name + '_' + count
    if (n) {
        subject += '_pub_' + n
    }
    if (m) {
        subject += '_sub_' + m
    }
    return subject;
}

function getMakeFolder() {
    return '../cmake-build-release'
}

function buildProject() {
    return shell.exec(`cd ${getMakeFolder()} && make`);
}


//TODO: überall ändern
/**
 *
 * @param msgPerSecond
 * @param subject
 * @param output
 * @param verbose
 * @param async
 * @returns {*}
 */
function startNatsSubscriber(subject, output, verbose = {active: false, id: 0},   async = true, url = natsUrl) {
    //console.log('out', `${getMakeFolder()}/nats_subscriber --subject ${subject} --url ${url} --output ${output} ${verbose.active ? ('--verbose ' + verbose.id) : ''}`, {async: async})
    return shell.exec(`${getMakeFolder()}/nats_subscriber --subject ${subject} --url ${url} --output ${output} ${verbose.active ? ('--verbose ' + verbose.id) : ''}`, {async: async});
}

//TODO: überall ändern
/**
 *
 * @param msgPerSecond  messages per second
 * @param duration      duration of benchmark
 * @param msgLength     length of one message
 * @param subject       subject of channel
 * @param verbose
 * @param async
 * @returns {Promise<any>}
 */
async function startNatsPublisher(duration = 30, subject, min, max, steps, verbose = {
    active: false,
    id: 0
}, async = false, url = natsUrl) {
    return await exec(`${getMakeFolder()}/nats_publisher --duration ${duration} --subject ${subject} --min ${min} --max ${max} --steps ${steps} --url ${url} ${verbose.active ? '--verbose ' + verbose.id : ''} ${async ? '&' : ''}`);
}

async function startTest(name, numberOfPublishers = 1, numberOfSubscribers = 1, min = min, max = max, steps = steps) {
    if (!name) {
        console.log('no count or name set!');
        return;
    }

    const output = genOutputFolder(name, min + '-' + max + '_' + steps, numberOfSubscribers, numberOfPublishers)
    const subject = genSubject(name, min + '-' + max + '_' + steps, numberOfSubscribers, numberOfPublishers);

    console.log('---------------------------------------');
    console.log(`start ${name} test with message count ${min} to ${max}`);

    const allProcesses = []

    if(argSwitch === 'subscriber') {
        for (let i = 0; i < numberOfSubscribers; i++) {
            allProcesses.push(startNatsSubscriber(subject, output + '_' + i, {active: verbose, id: i + 1}, true, natsUrl));
        }
        await sleep(duration * (max/steps) + 20);
    }

    if(argSwitch === 'publisher') {
        for (let i = 0; i < numberOfPublishers; i++) {
            await startNatsPublisher(duration, subject, min, max, steps, {active: verbose, id: i + 1}, numberOfPublishers !== 1, natsUrl);
        }
        await sleep(duration * (max/steps) + 10);
    }

    console.log(`end ${name} test with message count  ${min} to ${max}`)
}

async function run1to1() {
    //for (const number of allNumbers) {
    await startTest("1-1", 1, 1, min, max, steps);
    //}
}

async function run1toN() {
    for (const n of allNs) {
        await startTest( "1-N", 1, n,  min, max, steps);
    }
}

async function runNto1() {
    for (const n of allNs) {
        await startTest("N-1", n, 1,  min, max, steps);
    }
}

async function runNtoN() {
    for (const number of allNumbers) {
        for (const n of allNs) {
            for (const m of allNs) {
                await startTest("N-N", number, n, m,  min, max, steps);
            }
        }
    }
}


async function runAll() {
    const argv = require("minimist")(process.argv.slice(2));

    if(argv.hasOwnProperty('publisher')) {
        argSwitch = "publisher";
    } else if(argv.hasOwnProperty('subscriber')) {
        argSwitch = "subscriber";
    }

    if(argv.hasOwnProperty("url")) {
        natsUrl = argv.url;
    }

    if(argv.hasOwnProperty("verbose")) {
        verbose = true;
    }

    if(argv.hasOwnProperty("min")) {
        if(argv.min <= 0) return;
        min = argv.min;
    }
    if(argv.hasOwnProperty("max")) {
        if(argv.max <= 0) return;
        max = argv.max;
    }
    if(argv.hasOwnProperty("step")) {
        if(argv.step <= 0) return;
        step = argv.step;
    }

    // console.log('min', min, max, step)

    //await buildProject();

     await run1to1();

    /*await run1toN();
    await runNto1();
    await runNtoN();*/
    //await killAll();
    // drawGraphs();
    // process.exit(0);
}

runAll();


