#!/bin/bash
cd scripts && npm i;

cd ..;

##cleanup
rm -rf build;
cd nats.c && rm -rf build;
NATS_HOME=$(pwd);
mkdir -p build && cd build;

cmake .. -DNATS_BUILD_STREAMING=OFF && make clean && make;

