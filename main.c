//
// Created by Maximilian Kamp on 20.03.20.
//


#include <stdio.h>
#include "data.h"
#include <unistd.h>
#include <pthread.h>


void *myFunction(void *vargp) {
    int timerCount = 0;
    int lastCounter = 0;
    int counter = 0;
    int i = 0;
    do {
        sleep(1);
        lastCounter = counter;
        printf("print\n");
        if(i < 3) {
            counter++;
        }

        if(lastCounter == counter) {
            timerCount++;
        } else {
            timerCount = 0;
        }

        printf("%d %d\n", timerCount, counter);
        i++;
    } while(timerCount != 10);
    return NULL;
}

int main ()  {
    pthread_t thread_id;
    printf("Before Thread\n");
    pthread_create(&thread_id, NULL, myFunction, NULL);
    pthread_join(thread_id, NULL);
    printf("After Thread\n");
    return 0;
}